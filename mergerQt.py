
import sys
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QHBoxLayout, QGroupBox, QVBoxLayout, QGridLayout, QListView, QListWidget, QFileDialog
from PyQt5.QtCore import pyqtSlot, Qt
import PyPDF2

class Aplikacja(QWidget):

    def __init__(self):
        super().__init__()
        self.tytul = "PDF Merger"
        self.lewa = 10
        self.prawa = 10
        self.gora = 10
        self.szerokosc = 640
        self.wysokosc = 320
        self.InitUI()

    def InitUI(self):
        self.setWindowTitle(self.tytul)
        self.setGeometry(self.lewa, self.gora, self.szerokosc, self.wysokosc)

        grupaHoryzontalna = QGroupBox("")

        kompozycja = QGridLayout()
        kompozycja.setColumnStretch(1, 6)
        kompozycja.setColumnStretch(2, 6)

        self.lista = QListWidget()

        kompozycja.addWidget(self.lista,0,0,3,3)

        bOtworz = QPushButton("Otwórz", self)
        bOtworz.clicked.connect(self.otworz)
        kompozycja.addWidget(bOtworz,6,1)

        bZapisz = QPushButton("Zapisz", self)
        bZapisz.clicked.connect(self.zapisz)
        kompozycja.addWidget(bZapisz,6,2)
  
        bUsun = QPushButton("Usuń", self)
        bUsun.clicked.connect(self.usun)
        kompozycja.addWidget(bUsun, 0, 4)

        grupaHoryzontalna.setLayout(kompozycja)

        self.setLayout(kompozycja)

        self.show()  
    
    def otworz(self):
        ls = []
        nazwa_pliku = QFileDialog()
        nazwa_pliku.setFileMode(QFileDialog.ExistingFiles)
        # otwarcie plików
        nazwy = nazwa_pliku.getOpenFileNames(self, "Otwórz pliki", "", "PDF (*.pdf)")

        # dodanie ścieżki plików do listy
        for nazwa in nazwy:
            ls.append(nazwa)
        
        self.lista.addItems(ls[0])
        
    def usun(self):
        item =self.lista.takeItem(self.lista.currentRow())
        item = None

    def zapisz(self):
        nazwa = QFileDialog.getSaveFileName(self, "Zapisz Plik","","PDF (*.pdf)")
        lista_nazw = [str(self.lista.item(i).text()) for i in range(self.lista.count())]
        writer = PyPDF2.PdfFileWriter()
        plik = open(nazwa[0] + '.pdf', "wb")
        infile = [] # lista pomocnicza
        # wczytanie wcześniej wybranych plików
        for nazwa_pliku in lista_nazw:
            p = open(nazwa_pliku, 'rb')
            reader = PyPDF2.PdfFileReader(p, strict=False)
            for strona in reader.pages:
                writer.addPage(strona)
            infile.append(p)
        
        # zapisanie i zamknięcie plików
        writer.write(plik)
        plik.close()
        for i in infile:
            i.close()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Aplikacja()
    sys.exit(app.exec_())