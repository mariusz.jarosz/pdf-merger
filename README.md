Prosty skrypt do scalania kilku plików PDF.

Program do poprawnego dziąłania wymaga zainstalowania pakietów PyQT5 i PyPDF2.

Po uruchomieniu programu należy za pomocą przycisku "Otwórz" wskazać pliki, których chcemy scalić. Następnie po naciśnięciu Zapisz, trzeba podać nazwę nowego pliku i zostanie utworzony scalony dokument pdf.

Program wydany jest na licencji GNU GPL, każdy ma możliwość modyfikowania, prosiłbym jedynie o dodanie informacji o twórcy. 

Program był tworzony w ramach nauki programowania w języku Python, a konkretnie pakietu PyQt5, PyPDF2, autor nie odpowiada za błędy wynikłe z jego użytkowania.

W razie pytań proszę pisać na: mariusz.jarosz@protonmail.com

Mariusz Jarosz




 
